#!/usr/bin/env python
# -*- coding: utf8 -*-

import traceback
import cx_Oracle as oracle

from config import oracle_db_config


def db_connection(user, password, host, name, read_only=True, port=1526):
    def decorator(func):
        def wrapper(*args, **kwargs):
            url = ('{user}/{passwd}@{host}:{port}/{dbname}'
                   ''.format(user=user, passwd=password, host=host, port=port, dbname=name))
            connection = oracle.Connection(url)
            print connection.version
            cursor = connection.cursor()
            try:
                if not read_only:
                    connection.begin()
                retval = func(cursor, *args, **kwargs)
                if not read_only:
                    connection.commit()
            except:
                print traceback.format_exc()
                if not read_only:
                    connection.rollback()
            finally:
                connection.close()
            return retval
        return wrapper
    return decorator


oracle_db_config.update({'read_only': False})
db = db_connection(**oracle_db_config)


@db
def create_table(cursor):
    sql_query = """CREATE TABLE vozidlo (vyrobce varchar2(64),
                                         model varchar2(64),
                                         foto ORDSYS.ORDImage default ORDSYS.ORDImage.Init(),
                                         foto_si ORDSYS.SI_StillImage,
                                         foto_ac ORDSYS.SI_AverageColor,
                                         foto_ch ORDSYS.SI_ColorHistogram,
                                         foto_pc ORDSYS.SI_PositionalColor,
                                         foto_tx ORDSYS.SI_Texture,
                                         primary key (vyrobce, model))"""
    cursor.execute(sql_query)


if __name__ == '__main__':
    create_table()
