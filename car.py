
from database import db


class Car(object):
    def __init__(self, brand, model, picture):
        super(Car, self).__init__()
        self.brand = brand
        self.model = model
        self.picture = picture

    @db
    def insert_to_db(cur, self):
        sql = """INSERT INTO vozidlo(vyrobce, model)
                 VALUES      (:v, :m)"""
        sql_data = {
            'v': self.brand,
            'm': self.model,
        }
        cur.execute(sql, sql_data)

    @db
    def load(cur, self):
        cur.execute("select * from vozidlo where vyrobce = 'opel'")
        result = cur.fetchone()
        print result

if __name__ == '__main__':
    a = Car('opel', 'astra')
    a.insert_to_db()
