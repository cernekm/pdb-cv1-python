#!/usr/bin/env python
# -*- coding: utf8 -*-

import os


oracle_db_config = {
    'user': os.environ.get('ORACLE_DB_USER'),
    'password': os.environ.get('ORACLE_DB_PASSWORD'),
    'host': 'berta.fit.vutbr.cz',
    'name': 'pdb1'
}
